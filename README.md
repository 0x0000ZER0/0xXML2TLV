# 0xXML2TLV

A simple program to convert from XML to TLV file format.

## Build

The program does not use external libraries which should make the build process easier. Simply type `make`.

## Usage

For converting type:

```
./xml2tlv ./MY_FILE.xml
``` 

Which after it'll create the `MY_FILE.tlv` file.

- testing example:

```
> cat ./test/input3.xml

<START>
        <text>string 1</text>
        <text>string 2</text>
        <text>string 3</text>
        <numeric>205011</numeric>
        <numeric>7</numeric>
</START>

> ./xml2tlv ./test/input3.xml
> hexdump ./test/input3.tlv

0001 0000 0008 0000 7473 6972 676e 3120
0001 0000 0008 0000 7473 6972 676e 3220
0001 0000 0008 0000 7473 6972 676e 3320
0000 0000 0008 0000 20d3 0003 0000 0000
0000 0000 0008 0000 0007 0000 0000 0000
