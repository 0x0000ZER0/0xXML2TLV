#ifndef DEFS_H
#define DEFS_H

#include <stdint.h>
#include <stddef.h>

enum {
	TLV_TYPE_NUM = 0,
	TLV_TYPE_STR
};

typedef struct {
	// NOTE: This could be 1 byte long, but because of padding it actually won't matter.
	//       One could also use bit fields to store it in one bit and reuse the rest of it.
	uint32_t  type; 
	uint32_t  len;
	union {
		uint64_t val_num;
		char	 *val_str;
	};
} tlv_item;

typedef struct {
	uint32_t cap;
	uint32_t len;
	tlv_item *items;
} tlv_ctx;

enum {
	TLV_INIT_OK = 0,
	TLV_INIT_ERR_MEM_ALLOC
};

uint32_t
tlv_init(tlv_ctx*);

enum {
	TLV_MEM_ALLOC_OK = 0,
	TLV_MEM_ALLOC_ERR_MEM_ALLOC
};

uint32_t
tlv_mem_alloc(tlv_ctx*, uint32_t);

enum {
	TLV_ADD_NUM_OK = 0,
	TLV_ADD_NUM_ERR_MEM_ALLOC
};

uint32_t
tlv_add_num(tlv_ctx*, uint64_t);

enum {
	TLV_ADD_STR_OK = 0,
	TLV_ADD_STR_ERR_MEM_ALLOC,
	TLV_ADD_STR_ERR_STR_ALLOC
};

uint32_t
tlv_add_str(tlv_ctx*, char*, uint32_t);

enum {
	TLV_FROM_XML_OK = 0,
	TLV_FROM_XML_ERR
};

uint32_t
tlv_from_xml(tlv_ctx*, char*, size_t);

enum {
	TLV_FREE_OK = 0
};

uint32_t
tlv_free(tlv_ctx*);

enum {
	TLV_EXPORT_OK = 0,
	TLV_EXPORT_ERR_FILE_OPEN,
	TLV_EXPORT_ERR_FILE_WRITE
};

uint32_t
tlv_export(tlv_ctx*, const char*);

void
tlv_print(tlv_ctx*);

#endif
