#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "defs.h"

static inline void
log_dbg(const char *fmt, ...)
{
#ifdef DEBUG
	va_list args;

	printf("DBG: ");
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
	printf(".\n");
#endif
}

static inline void
log_err(const char *txt) 
{
	fprintf(stderr, "ERR: %s.\n", txt);
}

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		log_err("you must specify the file path");
		goto _EXIT_;
	}	

	uint32_t res;

	tlv_ctx ctx;
	res = tlv_init(&ctx);
	if (res != TLV_INIT_OK) {
		log_err("failed to initialize the TLV context");
		goto _EXIT_;
	}

	int file;
	file = open(argv[1], O_RDONLY);
	if (file == -1) {
		log_err("failed to open the file");
		goto _TLV_FREE_;
	}

	ssize_t bytes;
	char buff[2048];
	bytes = read(file, buff, sizeof (buff)); 	
	if (bytes == -1) {
		log_err("failed to read from the file");
		goto _CLOSE_FILE_;
	}

	res = tlv_from_xml(&ctx, buff, bytes);
	if (res != TLV_FROM_XML_OK) {
		log_err("failed to parse the .xml file");
		goto _CLOSE_FILE_;
	}

#ifdef DEBUG
	tlv_print(&ctx);
#endif

	char *path;
	path = argv[1];
	size_t path_len;
	path_len = strlen(path);

	path[path_len - 1] = 'v';
	path[path_len - 2] = 'l';
	path[path_len - 3] = 't';
	log_dbg("%s", path);

	res = tlv_export(&ctx, path);
	if (res != TLV_EXPORT_OK) {
		log_err("failed to export the .tlv file");
		goto _CLOSE_FILE_;
	}

_CLOSE_FILE_:	
	close(file);
_TLV_FREE_:
	tlv_free(&ctx);
_EXIT_:
	return 0;
}
