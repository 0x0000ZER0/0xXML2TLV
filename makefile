SRC=main.c xml.c tlv.c
OUT=xml2tlv

.PHONY:release
release:
	gcc -Os $(SRC) -o $(OUT)

.PHONY:debug
debug:
	gcc -g -Wall -Wextra -fsanitize=leak,undefined,address $(SRC) -o $(OUT) -DDEBUG

.PHONY:clear
clear:
	rm $(OUT) tags
