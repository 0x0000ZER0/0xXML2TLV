#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "defs.h"

uint32_t
tlv_init(tlv_ctx *ctx)
{
	ctx->cap = 4;
	ctx->len = 0;

	ctx->items = malloc(sizeof (tlv_item) * ctx->cap);
	if (ctx->items == NULL)
		return TLV_INIT_ERR_MEM_ALLOC;
	

	return TLV_INIT_OK;
}

uint32_t
tlv_mem_alloc(tlv_ctx *ctx, uint32_t count)
{
	tlv_item *new_items;
	new_items = realloc(ctx->items, sizeof (tlv_item) * count);
	if (new_items == NULL)
		return TLV_MEM_ALLOC_ERR_MEM_ALLOC;

	ctx->items = new_items;
	return TLV_MEM_ALLOC_OK;
}

uint32_t
tlv_add_num(tlv_ctx *ctx, uint64_t val)
{
	if (ctx->cap == ctx->len) {
		ctx->cap *= 2;

		uint32_t res;		
		res = tlv_mem_alloc(ctx, ctx->cap);
		if (res != TLV_MEM_ALLOC_OK)
			return TLV_ADD_NUM_ERR_MEM_ALLOC;		
	}

	tlv_item *item;
	item = &ctx->items[ctx->len];

	item->type = TLV_TYPE_NUM;
	item->len  = sizeof (uint64_t);
	
	item->val_num = val;
	++ctx->len;	
	return TLV_ADD_NUM_OK;
}

uint32_t
tlv_add_str(tlv_ctx *ctx, char *val, uint32_t val_len)
{
	if (ctx->cap == ctx->len) {
		ctx->cap *= 2;

		uint32_t res;		
		res = tlv_mem_alloc(ctx, ctx->cap);
		if (res != TLV_MEM_ALLOC_OK)
			return TLV_ADD_STR_ERR_MEM_ALLOC;		
	}

	tlv_item *item;
	item = &ctx->items[ctx->len];

	item->type = TLV_TYPE_STR;
	item->len  = val_len;
	
	item->val_str = malloc(val_len);
	if (item->val_str == NULL)
		return TLV_ADD_STR_ERR_STR_ALLOC;

	memcpy(item->val_str, val, val_len);

	++ctx->len;	
	return TLV_ADD_STR_OK;
}

uint32_t
tlv_free(tlv_ctx *ctx)
{
	tlv_item *item;
	for (uint32_t i = 0; i < ctx->len; ++i) {
		item = &ctx->items[i];

		if (item->type == TLV_TYPE_STR)
			free(item->val_str);
	}

	free(ctx->items);

	return TLV_FREE_OK;
}

uint32_t
tlv_export(tlv_ctx *ctx, const char *path)
{
	int flags;
	flags = O_WRONLY | O_CREAT | O_TRUNC;

	int file;
	file = open(path, flags, S_IWUSR | S_IRUSR);

	printf("---;-- O --;----\n");
	if (file == -1)
		return TLV_EXPORT_ERR_FILE_OPEN;

	tlv_item *item;
	ssize_t bytes;
	for (uint32_t i = 0; i < ctx->len; ++i) {
		item = &ctx->items[i];		

		bytes = write(file, &item->type, sizeof (item->type));
		if (bytes != sizeof (item->type)) {
			close(file);
			return TLV_EXPORT_ERR_FILE_WRITE;
		}

		bytes = write(file, &item->len, sizeof (item->len));
		if (bytes != sizeof (item->len)) {
			close(file);
			return TLV_EXPORT_ERR_FILE_WRITE;
		}

		switch (item->type) {
		case TLV_TYPE_NUM:
			bytes = write(file, &item->val_num, item->len);
			break;
		case TLV_TYPE_STR:
			bytes = write(file, item->val_str, item->len);
			break;
		}

		if (bytes != item->len) {
			close(file);
			return TLV_EXPORT_ERR_FILE_WRITE;
		}
	}

	close(file);
	return TLV_EXPORT_OK;
}

void
tlv_print(tlv_ctx *ctx)
{
	const char *type_str[] = { "NUM", "STR" };

	printf("CAP: %u\n", ctx->cap);
	printf("LEN: %u\n", ctx->len);

	tlv_item *item;
	for (uint32_t i = 0; i < ctx->len; ++i) {
		item = &ctx->items[i];

		printf("------------------ %u ------------------\n", i);
		printf("TYPE: %s\n", type_str[item->type]);
		printf("LEN:  %u\n", item->len);
		switch (item->type) {
		case TLV_TYPE_NUM:
			printf("VAL:  %lu\n", item->val_num);
			break;
		case TLV_TYPE_STR:
			printf("VAL:  %.*s(%u)\n", (int)item->len, item->val_str, item->len);
			break;
		}
	}
}
