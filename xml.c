#include <string.h>
#include <stdio.h>

#include "defs.h"

static char*
xml_skip(const char *in) 
{
	char *out;
	out = (char*)in;

	while (*out == ' ' || *out == '\t' || 
		*out == '\n' || *out == '\r') {
		++out;
	}

	return out;
}

static char*
xml_opening_tag(const char *in, char *tag, size_t *len)
{
	char *curr;
	curr = xml_skip(in);
	if (*curr != '<')
		return NULL;

	++curr;

	size_t i;
	for (i = 0; *curr != '>' && i < *len; ++i) {
		tag[i] = *curr;
		++curr;
	}

	if (*curr != '>')
		return NULL;
	
	*len = i;
	++curr;

	return curr;
}

static char*
xml_parse_entry(const char *in, char *val, size_t *len)
{
	char *curr;
	curr = xml_skip(in);

	size_t i;
	for (i = 0; *curr != '<' && i < *len; ++i) {
		val[i] = *curr;
		++curr;
	}

	if (*curr != '<')
		return NULL;

	*len = i;

	return curr;	
}

static char*
xml_closing_tag(const char *in, char *tag, size_t *len)
{
	char *curr;
	curr = xml_skip(in);
	if (*curr != '<') 
		return NULL;
	
	++curr;
	if (*curr != '/') 
		return NULL;

	++curr;

	size_t i;
	for (i = 0; *curr != '>' && i < *len; ++i) {
		tag[i] = *curr;
		++curr;
	}

	if (*curr != '>')
		return NULL;
	
	*len = i;
	++curr;

	return curr;
}

static char*
xml_parse_content(tlv_ctx *ctx, char *xml)
{
	const char tag_num[] = "numeric";
	const char tag_str[] = "text";

	// opening tag
	char o_tag[32];
	size_t o_tag_len;
	// entry
	char val[64];
	size_t val_len;
	// closing tag
	char c_tag[32];		
	size_t c_tag_len;

	char *curr;
	curr = xml;
	do {
		o_tag_len = sizeof (o_tag);
		val_len = sizeof (val);
		c_tag_len = sizeof (c_tag);

		curr = xml_opening_tag(curr, o_tag, &o_tag_len);
		if (curr == NULL)
			return NULL;

		if (o_tag[0] == '/') {
			curr -= 3 + o_tag_len;
			break;
		}

		curr = xml_parse_entry(curr, val, &val_len);
		if (curr == NULL)
			return NULL;

		curr = xml_closing_tag(curr, c_tag, &c_tag_len);
		if (curr == NULL)
			return NULL;

		if (o_tag_len != c_tag_len) {
			return NULL;
		}
		
		if (o_tag_len != (sizeof (tag_num) - 1) &&
			o_tag_len != (sizeof (tag_str) - 1)) {
			return NULL;
		}

		if (memcmp(o_tag, c_tag, o_tag_len) != 0)
			return NULL;

		if (o_tag_len == (sizeof (tag_str) - 1) && 
			memcmp(o_tag, tag_str, o_tag_len) == 0) {
			tlv_add_str(ctx, val, val_len);
		} else if (memcmp(o_tag, tag_num, o_tag_len) == 0) {
			uint64_t val_num;
			val_num = 0;

			for (size_t i = 0; i < val_len; ++i) {
				val_num *= 10;
				val_num += val[i] - '0';
			}

			tlv_add_num(ctx, val_num);
		} else {
			return NULL;
		}
	} while (1);

	return curr;
}

uint32_t
tlv_from_xml(tlv_ctx *ctx, char *xml, size_t xml_len)
{
	// NOTE: was meant to be used for extra error checking.
	(void)xml_len;

	const char tag_start[] = "START";

	char tag[32];
	size_t tag_len = sizeof (tag);

	char *curr;
	curr = xml_opening_tag(xml, tag, &tag_len);
	if (memcmp(tag, tag_start, tag_len) != 0)
		return TLV_FROM_XML_ERR;

	curr = xml_parse_content(ctx, curr);
	if (curr == NULL)
		return TLV_FROM_XML_ERR;

	curr = xml_closing_tag(curr, tag, &tag_len);
	if (memcmp(tag, tag_start, tag_len) != 0)
		return TLV_FROM_XML_ERR;

	return TLV_FROM_XML_OK;
}

